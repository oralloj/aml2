'use_strict';

function getDetails(params){
    /**
     * Llamada al webservice. Sustituimos la llamada al WS por una respuesta mockeada
     */
    //var result = queriesRepository.getDetails(params);
    return new Promise((resolve, reject) => {
        try{
            var result = [
                {
                    unit: 'UK',
                    ratio_match: '90',
                    num_match: '1',
                    action: 'Desvincular',
                    name: '',
                    lastname: '',
                    id_type: '',
                    id_code: '',
                    nationality: '',
                    address: '',
                    dob_day: '',
                    dob_month: '',
                    dob_year: ''
                }, 
                {
                    unit: 'ES',
                    ratio_match: '',
                    num_match: '',
                    action: 'Monitorizar',
                    name: 'John',
                    lastname: 'Doe',
                    id_type: 'DNI',
                    id_code: '11223344A',
                    nationality: 'GB',
                    address: 'Fake St. 123',
                    dob_day: '12',
                    dob_month: '09',
                    dob_year: '1975' 
                }
            ];
            resolve(result);
        } catch (err){
            reject(err);
        }
    })
}

function getClients(params){
    /**
    * Llamada al webservice. Sustituimos la llamada al WS por una respuesta mockeada
    */
    //var result = queriesRepository.getClients(params);
    return new Promise((resolve, reject) => {
        try{
            var result = {
                unit: 'ES',
                risk: 'a',
                person_type: 'J',
                address: 'Address 1',
                nationality: 'SPA',
                num_match: '1'
            };
            resolve(result);
        } catch (err){
            reject(err);
        }
    })
    
}

function getExpedients(params){
    /**
     * Llamada al webservice. Sustituimos la llamada al WS por una respuesta mockeada
     */
    //var result = queriesRepository.getExpedients(params);
    return new Promise((resolve, reject) => {
        try{
            var result = {
                unit: 'UK',
                status: 'A',
                num_expedients: '2'
            };
            resolve(result);
        } catch (err){
            reject(err);
        }
    })
}

module.exports = {
    getDetails,
    getClients,
    getExpedients
}