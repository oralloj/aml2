'use strict';

var Log               = require('log');
var _                 = require('lodash');

//var appProperties   = require('../helpers/app.properties');

//var log = new Log(appProperties.getAppConfig().logLevel);
var nameModule = "[Controller Helper]";

////////////////////////////////////////////////////////////////////////////////
// HANDLE EVENTS FOR ERRORS
////////////////////////////////////////////////////////////////////////////////

function buildErrorLog(err) {
  var errorLog;
  if (!_.isUndefined(err.stack)) {
    errorLog = err.stack;
  } else if (!_.isUndefined(err)) {
    errorLog = JSON.stringify(err);
  } else {
    errorLog = 'Error no definido';
  }
  return errorLog;
}


function handleErrorResponse(nameController, nameMethod, err, res) {
  log.error(`-----> ${nameController} ${nameMethod} (ERROR) -> Error: ${JSON.stringify(err)} `);

  //Caso en el que nos viene ya preparado el error con su code
  if (!_.isUndefined(err.code)) {

    if ('Incorrect authentication.' === err.message) {
      err.message = 'Autenticación Incorrecta.';
    }
    var jsonResultField = {
      error: err
    };
    res.status(jsonResultField.error.code).send(jsonResultField);
  } else {
    handleGenericErrorResponse(nameController, nameMethod, err, res)
  }
}

function handleGenericErrorResponse(nameController, nameMethod, err, res) {
  log.error(`-----> ${nameController} ${nameMethod} (ERROR) -> Error: ${buildErrorLog(err)} `);

  var jsonResultFailed = {
    error: {
      code: 500,
      message: `Error Interno de Aplicación en ${nameMethod}`
    }
  }

  res.status(500).send(jsonResultFailed);
}

function handleTimeOutErrorResponse(nameController, nameMethod, err, res) {
  log.error(`-----> ${nameController} ${nameMethod} (ERROR) -> Error: ${buildErrorLog(err)} `);

  var jsonResultFailed = {
    error: {
      code: 504,
      message: `Ha expirado el tiempo máximo de ejecución`
    }
  }

  res.status(504).send(jsonResultFailed);
}

function handleGenericDataResponse(nameController, nameMethod, data, res) {
  console.log('dentro helper');
  //log.info(`-----> ${nameController} ${nameMethod} (OUT) -> data: ${JSON.stringify(data)} `);
  var jsonDataResult = {
    data: data
  }
  //console.log();
  res.status(200).send(jsonDataResult);
}

module.exports = {
  handleErrorResponse: handleErrorResponse,
  handleGenericErrorResponse: handleGenericErrorResponse,
  handleGenericDataResponse: handleGenericDataResponse,
  handleTimeOutErrorResponse: handleTimeOutErrorResponse
};
