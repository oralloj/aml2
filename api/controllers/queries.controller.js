'use strict';

var util = require('util');
var queriesService = require('../services/queries.service');

var controllerHelper = require('../helpers/controller.helper');

module.exports = {
  getDetails: getDetails,
  getClients: getClients,
  getExpedients: getExpedients
};


var nameController = 'queriesController';

/*
  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getDetails(req, res) {
  var nameMethod = 'getDetails';
  try{

    var parameters = {
      name: req.swagger.params.name.value || '',
      fuzzy_name: req.swagger.params.fuzzy_name.value || '',
      lastname: req.swagger.params.lastname.value || '',
      fuzzy_lastname: req.swagger.params.fuzzy_lastname.value || '',
      id_type: req.swagger.params.id_type.value || '',
      id_code: req.swagger.params.id_code.value || '',
      nationality: req.swagger.params.nationality.value || '',
      address: req.swagger.params.address.value || '',
      dob_date: req.swagger.params.dob_date.value || '',
      dob_year: req.swagger.params.dob_year.value || ''
    }

    queriesService.getDetails(parameters)
      .then(result => {
        controllerHelper.handleGenericDataResponse(nameController, nameMethod, result, res);
      })
      .catch();;


  } catch (err){
    controllerHelper.handleErrorResponse(nameController, nameMethod, err, res);
  }
  
}

/*
  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getClients(req, res) {
  var nameMethod = 'getClients';
  try{
    
    var parameters = {
      risk_level: req.swagger.params.risk_level.value || '',
      nationality: req.swagger.params.nationality.value || '',
      address: req.swagger.params.address.value || '',
      person_type: req.swagger.params.person_type.value || ''
    };
    console.log('antes service');
    queriesService.getClients(parameters)
      .then(result => {
        console.log('dentro service');
        controllerHelper.handleGenericDataResponse(nameController, nameMethod, result, res);
      })
      .catch();

  } catch (err){
    controllerHelper.handleErrorResponse(nameController, nameMethod, err, res);
  }

}

/*
  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function getExpedients(req, res) {
  var nameMethod = 'getExpedients';
  try{
    
    var parameters = {
      start_date: req.swagger.params.start_date.value || '',
      end_date: req.swagger.params.end_date.value || '',
      status: req.swagger.params.status.value || ''
    };

    queriesService.getExpedients(parameters)
      .then(result => {
        controllerHelper.handleGenericDataResponse(nameController, nameMethod, result, res);
      })
      .catch();

  } catch (err){
    controllerHelper.handleErrorResponse(nameController, nameMethod, err, res);
  }

}

