'use_strict';

var _ = require('lodash');

var clients = [];

function getClients(params){

    clientsResult = clients.slice();

    // Filter by name
    if (params.name !== undefined){
        clientsResult = _.filter(clients, { name: params.name });
    }

    return clientesResult;

}

module.exports = {
    getClients
}